let ws = new WebSocket("ws://localhost:5000");
ws.binaryType = "arraybuffer";

$("#status").text("connecting...")
ws.onopen  = event => $("#status").text("online");
ws.onclose = event => $("#status").text("offline");

$("#send").click(function() {
    let x = new ArrayBuffer(8);
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;
    x[5] = 5;
    x[6] = 6;
    x[7] = 7;
    ws.send(x);
});
