#include "crypto.h"
#include <inttypes.h>

int base64_encode(char *dest, size_t dest_len, const char *src, size_t src_len) {
    const char *charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    unsigned char triple[3];
    int triple_iter;
    int len;
    int done = 0;

    while (src_len) {
        len = 0;
        for (triple_iter = 0; triple_iter < 3; triple_iter++) {
            if (src_len) {
                triple[triple_iter] = *src++;
                len++;
                src_len--;
            } else {
                triple[triple_iter] = 0;
            }
        }

        if (done + 4 >= dest_len)
            return 1;

        *dest++ = charset[triple[0] >> 2];
        *dest++ = charset[((triple[0]&0x03)<<4) | ((triple[1]&0xf0)>>4)];
        *dest++ = (len > 1 ? charset[((triple[1]&0x0f)<<2) | ((triple[2]&0xc0)>>6)] : '=');
        *dest++ = (len > 2 ? charset[triple[2]&0x3f]:'=');

        done += 4;
    }

    if (done + 1 >= dest_len)
        return 1;

    *dest++ = '\0';

    return 0;
}
