#ifndef CRYPTO_H
#define CRYPTO_H

#include <stddef.h>

int base64_encode(char *dest, size_t dest_len, const char *src, const size_t src_len);

#endif
