#include "jws.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <openssl/sha.h>
#define RECV_BUFFER_LEN 512 
#define MAGIC_STRING "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define B64_BUFFER_LEN 64
#define REPLY_BUFFER_LEN 256

#include "crypto.h"

// TODO pthread cancel states

struct jws_server_t {
    int fd;
    pthread_t thread;

    int clients_len;
    jws_client_t *clients;
};

struct jws_client_t {
    int fd;

    pthread_t thread;
    char *recv_buffer;

    char active;
};

struct jws_packet_t {
    size_t len;
    void *data;
};

jws_server_t *jws_server_make(int clients_len) {
    int iter;
    // Create server and return.
    jws_server_t *server = malloc(sizeof(jws_server_t));

    server->clients_len  = clients_len;
    server->clients      = malloc(clients_len*sizeof(jws_client_t));
    for (iter = 0; iter < clients_len; iter++) {
        server->clients[iter].active = 0;
        server->clients[iter].recv_buffer = malloc(RECV_BUFFER_LEN);
    }

    return server;
}
void jws_server_destroy(jws_server_t *server) {
    int iter;
    for (iter = 0; iter < server->clients_len; iter++) {
        free(server->clients[iter].recv_buffer);
    }
    free(server->clients);
    free(server);
}

jws_client_t *jws_server_add_client(jws_server_t *server) {

    int iter,
        fd,
        status;
    jws_client_t *client;

    fd = accept(server->fd, NULL, 0);

    if (fd == -1) {
        perror("accept");
        return NULL; // Connection failed.
    }

    // Find an empty spot.
    for (iter = 0; iter < server->clients_len; iter++)
        if (!server->clients[iter].active)
            break;
    if (iter == server->clients_len)
        return NULL; // No room in the server.
    client = &(server->clients[iter]);

    client->fd = fd;

    // handshake before starting a new thread.
    if (jws_handshake(client) != 0) {
        perror("handshake");
        close(client->fd);
        return NULL;
    }

    // Start the new thread.
    status = pthread_create(
        &(client->thread),
        NULL,
        jws_thread_entry_client,
        client
    );
    if (status != 0) {
        perror("pthread_create");
        close(client->fd);
        return NULL;
    }

    client->active = 1;

    return client;
}

int jws_handshake(jws_client_t *client) {
    int len;
    char *key,
         *key_end;
    int key_len;
    unsigned char sha1_buffer[SHA_DIGEST_LENGTH];
    char reply_buffer[REPLY_BUFFER_LEN];
    char b64[256];

    // TODO No reason we have to use a per-client buffer for this.
    len = recv(client->fd, client->recv_buffer, RECV_BUFFER_LEN, 0);
    if (len < 0)    // Connection error.
        return 1;
    if (len == 0)   // Client disconnected.
        return 2;
    
    // TODO Validate headers.

    // Get the key.
    key = strstr(client->recv_buffer, "Sec-WebSocket-Key: ");
    if (key == NULL)
        return 3;
    key = strstr(key, " "); // We know there's a space because ^
    key++;
    if (key == NULL)
        return 3;
    key_end = strstr(key, "\r\n");
    if (key_end == NULL)
        return 3;
    *key_end = 0; // Null byte.
    key_len = key_end - key;

    // Concatenate the magic string.
    strncat(key, MAGIC_STRING, client->recv_buffer+RECV_BUFFER_LEN - key_end);
    key_len += strlen(MAGIC_STRING);
    // SHA1.
    SHA1((unsigned char *) key, key_len, (unsigned char *) sha1_buffer);
    if (base64_encode(b64, 256, (char *) sha1_buffer, SHA_DIGEST_LENGTH) != 0) {
        return 4;
    }

    // Reply buffer.
    snprintf(reply_buffer, REPLY_BUFFER_LEN, "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: %s\r\n\r\n", b64);

    len = send(client->fd, reply_buffer, strlen(reply_buffer), 0);
    if (len == -1)
        return 5;

    return 0;
}

int jws_server_start(jws_server_t *server, int port) {
    int fd;
    struct sockaddr_in server_addr;

    // Socket file descriptor.
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        perror("socket failed");
        return 1;
    }

    // Socket address.
    server_addr.sin_family      = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port        = htons(port);

    // Bind.
    if (bind(fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) == -1) {
        perror("bind failed");
        return 1;
    }

    server->fd = fd;
    listen(server->fd, server->clients_len);
    pthread_create(&(server->thread), NULL, jws_thread_entry_server, server);

    return 0;
}

void jws_server_stop(jws_server_t *server) {
    int iter;
    // Cancel threads.
    for (iter = 0; iter < server->clients_len; iter++)
        if (server->clients[iter].active)
            pthread_cancel(server->clients[iter].thread); // TODO This could be better.
    // Close file descriptors.
    for (iter = 0; iter < server->clients_len; iter++)
        close(server->clients[iter].fd);
    close(server->fd);
}

void *jws_thread_entry_server(void *void_server) {
    jws_server_t *server = void_server;
    jws_client_t *client;

    while (1) {
        client = jws_server_add_client(server);
        if (client == NULL) // Server is full or connection failed.
            continue;
    }

    return NULL;
}

void *jws_thread_entry_client(void *void_client) {

    jws_packet_t packet;
    jws_client_t *client = void_client;
    int len;

    // Recv.
    while (1) {

        len = recv(client->fd, client->recv_buffer, RECV_BUFFER_LEN, 0);
        if (len < 0) {
            perror("recv");
            client->active = 0;
            close(client->fd);
            return NULL;
        }
        if (len == 0) { // Client disconnected.
            client->active = 0;
            close(client->fd);
            return NULL;
        }

        if (len > 0) {
            packet = jws_unpack_packet(client->recv_buffer, RECV_BUFFER_LEN);
        }
    }

    return NULL;
}

jws_packet_t jws_unpack_packet(char *buffer, size_t buffer_len) {
    jws_packet_t packet;
    uint64_t len;
    uint32_t *mask;
    int iter;

    // TODO
    // if bit 0 == 1
    //     close connection
    // Get the length.
    //     len =  bits 9-15 (as an unsigned integer)
    //     if len == 126
    //         len = next 16 bits
    //     if len == 127
    //         len = next 64 bits
    // if bit 8 == 0
    //     fail
    // mask = bits 80-112 (32 bits)
    // for 0..len
    //     decoded[i] = encoded[i] & mask[i%4]    

    packet.data = NULL;
    if (buffer_len <= 112) // Too short.
        return packet;
    if (buffer[0] == 0) // Fin.
        return packet;
    if (buffer[1] == 0) // Mask.
        return packet;
    if (((*buffer >> 4) & 0xF) == 2) // Must be binary.
        return packet;

    len = (uint64_t) (buffer[1] & 0x7F);
    if (len == 126) {
        len = ((uint16_t *)buffer)[1];
    } else if (len == 127) {
        len = *((uint64_t *)&buffer[2]);
    }

    mask = (uint32_t *) &buffer[10];
    
    // Decode the data.
    for (iter = 0; iter < len; iter++)
        buffer[iter+14] ^= mask[iter%4];

    printf("%lu\n", len);

    // TODO Inspect the sent data somehow.
    printf("\"");
    for (iter = 0; iter < buffer_len; iter++)
        printf("%d", buffer[iter]);
    printf("\"\n");

    return packet;
}
