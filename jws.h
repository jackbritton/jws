#ifndef JWS_H
#define JWS_H

#include <pthread.h>

struct jws_server_t;
typedef struct jws_server_t jws_server_t;

struct jws_client_t;
typedef struct jws_client_t jws_client_t;

// jws_server_t constructor / destructor
jws_server_t *jws_server_make(int clients_len);
void jws_server_destroy(jws_server_t *server);

// starts the accept thread
int jws_server_start(jws_server_t *server, int port);

// stop
void jws_server_stop(jws_server_t *server);
// Add a client, handshake, and start their thread.
jws_client_t *jws_server_add_client(jws_server_t *server);
int jws_handshake(jws_client_t *client);

// Entry point for the accept thread.
void *jws_thread_entry_server(void *void_server);

// Entry point for client threads.
void *jws_thread_entry_client(void *void_client);

// Packets.
struct jws_packet_t;
typedef struct jws_packet_t jws_packet_t;
// Unpacking.
jws_packet_t jws_unpack_packet(char *buffer, size_t buffer_len);

#endif
