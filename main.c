#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "jws.h"

// Ctrl-C catch.
volatile char stop = 0;
void sigint_handler(int _) {
    stop = 1;
}

int main(int argc, char *argv[]) {

    int port;
    jws_server_t *server;

    if (argc < 2) {
        printf("Usage: %s PORT\n", argv[0]);
        return 1;
    }

    // Get port and create the server.
    port = strtol(argv[1], NULL, 10);
    server = jws_server_make(4);
    if (server == NULL)
        return 1;

    // Set up our signal handler.
    signal(SIGINT, sigint_handler);

    if (jws_server_start(server, port) != 0)
        return 1;

    // Our main thread is now free to go on and do other things.
    while (!stop);

    jws_server_stop(server);
    jws_server_destroy(server);

    return 0;
}
