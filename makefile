CC = gcc
CFLAGS = -c -Wall
LFLAGS = -pthread -lcrypto
TARGET = server
OBJS = main.o jws.o crypto.o
DEPS = jws.h crypto.h

$(TARGET) : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(TARGET)

%.o : %.c $(DEPS)
	$(CC) $(CFLAGS) $< -o $@

clean :
	rm $(OBJS) $(TARGET)
